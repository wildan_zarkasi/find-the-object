﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveManager : MonoBehaviour
{
    bool once = true;

    string[] starName = new string[] { 
        "star_1",
        "star_2",
        "star_3",
    };

    string[] lastLevel = new string[]
    {
        "B-5",
        "H-5",
        "T-5",
    };  

    [Header("Control Save Data")]
    public bool clearSaveData = false;

    [Header("Attribute")]
    public GameObject lockPanel;
    public string[] levelName;
    public Sprite[] starSprite;
    public GameObject[] level;

    const string SCORE_KEY = "score";
    const string UNLOCKABLE_KEY = "unlockable";
    const string SOUND_KEY = "sound";
    const string ENABLE_SOUND_KEY = "enable_sound";

    public static void SaveLevelData(string key, int score, string nextLevel)
    {
        int previousScore = LoadLevelData(key);

        if (previousScore >= score) return;
        
        PlayerPrefs.SetInt(key + SCORE_KEY, score);
        PlayerPrefs.SetInt(nextLevel + UNLOCKABLE_KEY, 1);
        PlayerPrefs.Save();
    }

    public static int LoadLevelData(string key)
    {
        if (PlayerPrefs.HasKey(key + SCORE_KEY))
        {
            return PlayerPrefs.GetInt(key + SCORE_KEY);
        }
        else
        {
            return -1;
        }
    }

    public static void SaveSoundData(float volume, bool enableSound)
    {
        if (!enableSound)
        {
            PlayerPrefs.SetInt(ENABLE_SOUND_KEY, 0);
        }
        else
        {
            PlayerPrefs.SetInt(ENABLE_SOUND_KEY, 1);
        }

        PlayerPrefs.SetFloat(SOUND_KEY, volume);
        PlayerPrefs.Save();
    }

    public static float LoadSoundData()
    {
        if (PlayerPrefs.HasKey(SOUND_KEY)) return PlayerPrefs.GetFloat(SOUND_KEY);
        else return 1;
    }

    public static int LoadEnableSoundData()
    {
        if (PlayerPrefs.HasKey(ENABLE_SOUND_KEY)) return PlayerPrefs.GetInt(ENABLE_SOUND_KEY);
        else return 1;
    }

    private int LoadLevelUnlockableData(string key)
    {
        return PlayerPrefs.GetInt(key + UNLOCKABLE_KEY);
    }

    private void Update()
    {
        if(once)
        StartCoroutine(CheckSave());
    }

    IEnumerator CheckSave()
    {
        once = false;
        InitDataForTheFirstTime();
        if (clearSaveData) ClearSaveData();
        CheckLevelExtra();
        LoadLevelMainMenu();
        yield break;
    }

    void InitDataForTheFirstTime()
    {
        if (!PlayerPrefs.HasKey(levelName[0] + UNLOCKABLE_KEY)) InitSaveData();
    }

    void ClearSaveData()
    {
        PlayerPrefs.DeleteAll();
        InitSaveData();
    }

    void InitSaveData()
    {
        for(int i = 0; i < level.Length; i++)
        {
            if (i == 0 || i == 5 || i == 10) PlayerPrefs.SetInt(levelName[i] + UNLOCKABLE_KEY, 1);
            else PlayerPrefs.SetInt(levelName[i] + UNLOCKABLE_KEY, 0);
        }
    }

    public void LoadLevelMainMenu()
    {
        for(int i = 0; i < level.Length; i++)
        {
            if (LoadLevelUnlockableData(levelName[i]) == 0)
            {
                GameObject lockClone = Instantiate(lockPanel, level[i].transform);
                level[i].GetComponent<Button>().interactable = false;
            }
            else
            {
                int score = LoadLevelData(levelName[i]);
                SetStarUI(level[i], score);
            }
        }
    }

    public void SetStarUI(GameObject level, int score)
    {
        for(int i = 0; i < score; i++)
        {
            GameObject star = level.transform.Find(starName[i]).gameObject;
            star.GetComponent<Image>().sprite = starSprite[i];
        }
    }

    private bool CheckLevelExtra()
    {
        int score = 0;
        int unlock = 0;

        foreach (string level in lastLevel)
        {
            unlock = PlayerPrefs.GetInt(level + UNLOCKABLE_KEY);

            if (unlock == 0) return false;

            else
            {
                score = PlayerPrefs.GetInt(level + SCORE_KEY);

                if (score == 0) return false;
            }
        }

        PlayerPrefs.SetInt("E-1" + UNLOCKABLE_KEY, 1);
        return true;
    }
}