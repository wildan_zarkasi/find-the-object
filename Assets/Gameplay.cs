﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Gameplay : MonoBehaviour
{
    public LevelData levelData;
    private float timeGame;
    private int answerIndex = 0;
    private GameplayUI gameplayUI;

    public GameObject checkedThings;
    public GameObject canvasObject;
    public AudioManager audioManager;

    [Header("Game Answer Button")]
    public Button[] answerButtons;

    private List<int> answerLeftIndex = new List<int>();

    bool gameIsDone = false;

    private IEnumerator waitForAudioCorrectNumerator;

    private bool firstTime = true;

    private void Start()
    {
        gameplayUI = GetComponent<GameplayUI>();
        timeGame = levelData.levelTime;
        
        AddAnswerListIndex();
        RandomAnswerIndex();
    }

    private void Update()
    {
        if (CheckGameIsDone()) CheckWinRating();
        else if (CheckGameIsLose()) ShowLose();
        else CountGameTimer();
    }

    private void CountGameTimer()
    {
        if (!gameIsDone)
        {
            timeGame -= Time.deltaTime;
            gameplayUI.UpdateTimerUI(timeGame);
        }
    }

    private void RandomAnswerIndex()
    {
        if (answerLeftIndex.Count <= 0) return;
        
        bool isFound = false;

        while (!isFound)
        {
            answerIndex = UnityEngine.Random.Range(0, levelData.itemName.Length);
            isFound = answerLeftIndex.Contains(answerIndex);
        }

        gameplayUI.SetFindItemTextFooter(levelData.itemName[answerIndex]);
    }

    private void AddAnswerListIndex()
    {
        for(int i = 0; i < levelData.itemName.Length; i++)
        {
            answerLeftIndex.Add(i);
        }
    }

    private void RemoveAnswerListIndex(int index)
    {
        answerLeftIndex.Remove(index);
    }

    public void CheckAnswer(int index)
    {   
        if(index == answerIndex)
        {
            AnswerCorrect(index);
        }
        else
        {
            AnswerFalse();
        }
    }

    private void AnswerCorrect(int index)
    {
        RemoveAnswerListIndex(index);
        RemoveAnswerButton(index);
        
        RandomAnswerIndex();
        StartCoroutine(gameplayUI.ShowCorrectPanel(levelData.itemSprite[index],
            levelData.itemName[index],
            CorrectPanelDuration(index)
            )); ;
        audioManager.PlayAudioCorrect();

        waitForAudioCorrectNumerator = WaitAudioCorrectToFinish(index);
        StartCoroutine(waitForAudioCorrectNumerator);
    }

    private float CorrectPanelDuration(int index)
    {
        if (firstTime)
        {
            firstTime = false;
            return audioManager.AudioLength(index) + audioManager.audioOffset + audioManager.firstTimeOffset;
        }
        else
        {
            return audioManager.AudioLength(index) + audioManager.audioOffset;
        }
            
    }

    IEnumerator WaitAudioCorrectToFinish(int index)
    {
        audioManager.SetVolumeBGM(audioManager.defaultVolumeBGM / audioManager.decreaseAudioBGM);
        while (audioManager.IsAudioCorrectPlaying() == true) 
        {
            yield return new WaitForSeconds(0.01f);
        }

        audioManager.PlayAudioThing(index);

        while(audioManager.IsAudioThingsPlaying() == true)
        {
            yield return new WaitForSeconds(0.01f);
        }

        audioManager.SetVolumeBGM(audioManager.defaultVolumeBGM);
        yield return null;
    }

    private void RemoveAnswerButton(int index)
    {
        //answerButtons[index].interactable = false;
        GameObject correct = Instantiate(checkedThings, answerButtons[index].transform.position, Quaternion.identity);
        correct.transform.SetParent(canvasObject.transform);
        correct.transform.position = new Vector3(correct.transform.position.x, correct.transform.position.y, 0.5f);
        correct.SetActive(true);

        if (answerButtons[index].transform.Find("GFX"))
        {
            Image image = answerButtons[index].transform.Find("GFX").GetComponent<Image>();
            image.color = Color.gray;
        }
        else
        {
            Image image = answerButtons[index].GetComponent<Image>();
            image.color = Color.gray;
        }
    }

    private void AnswerFalse()
    {
        audioManager.PlayAudioFalse();
        StartCoroutine(gameplayUI.ShowFalsePanel());
    }

    private bool CheckGameIsDone()
    {
        return answerLeftIndex.Count <= 0;
    }

    private bool CheckGameIsLose()
    {
        if (timeGame > 0) return false;
        else return true;
    }

    private void CheckWinRating()
    {
        if (gameIsDone) return;
        StartCoroutine(gameplayUI.ShowWinPanel(CountRating()));
        gameIsDone = true;
        audioManager.PauseBGM();
    }

    private void ShowLose()
    {
        if (gameIsDone) return;
        gameplayUI.ShowLosePanel();
        gameIsDone = true;
        audioManager.PlayAudioLose();
        audioManager.PauseBGM();
    }

    private int CountRating()
    {
        int ratingSpan = Mathf.RoundToInt(levelData.levelTime / 3);

        for(int i = 3; i > 0; i--)
        {
            if (timeGame >= (ratingSpan * (i - 1)))
            {
                return i;
            }
        }
        return 0;
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
    }
}
