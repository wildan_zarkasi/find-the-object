﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneChanger : MonoBehaviour
{
    public Animator transitionAnimator;

    public void LoadScene(string scene)
    {
        transitionAnimator.SetTrigger("transition");
        StartCoroutine(WaitLoad(0.5f, 0, scene));
        Time.timeScale = 1;
    }

    public void RestartScene()
    {
        transitionAnimator.SetTrigger("transition");
        StartCoroutine(WaitLoad(0.5f, SceneManager.GetActiveScene().buildIndex));
        Time.timeScale = 1;
    }

    public void ToMenu()
    {
        transitionAnimator.SetTrigger("transition");
        StartCoroutine(WaitLoad(0.5f, 0));   
        Time.timeScale = 1;
    }

    IEnumerator WaitLoad(float second, int index, string scene = null)
    {
        yield return new WaitForSeconds(second);

        if(scene == null)
        {
            SceneManager.LoadScene(index);
        }
        else
        {
            SceneManager.LoadScene(scene);
        }
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
