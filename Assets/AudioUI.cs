﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioUI : MonoBehaviour
{
    public AudioManager audioManager;

    public Slider sliderVolume;
    public Toggle toggleSound;

    private void Awake()
    {
        InitVolumeUI();
    }

    public void InitVolumeUI()
    {
        float volume = SaveManager.LoadSoundData();
        int enableSound = SaveManager.LoadEnableSoundData();
        bool toggle;

        sliderVolume.value = volume;

        if (enableSound == 1) toggle = true;
        else toggle = false;

        toggleSound.isOn = toggle;

        sliderVolume.interactable = toggle;
        audioManager.SetVolumeSound(volume, toggle);
    }

    public void ChangeVolume()
    {
        bool enableSound = true;
        audioManager.SetVolumeSound(sliderVolume.value, enableSound);
    }

    public void EnableSound()
    {
        bool enableSound = false;

        if (!toggleSound.isOn)
        {
            enableSound = false;
            sliderVolume.interactable = false;
        }
        else
        {
            sliderVolume.interactable = true;
            enableSound = true;
        }

        audioManager.SetVolumeSound(sliderVolume.value, enableSound);
    }
}
