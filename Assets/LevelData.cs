﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Level Data", menuName = "Data/Level Data", order = 1)]
public class LevelData : ScriptableObject
{
    public string levelName;
    public string levelNextName;
    public float levelTime;
    public string[] itemName;
    public Sprite[] itemSprite;
    public AudioClip[] audioThingClip;
}