﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    [HideInInspector] public float firstTimeOffset = 0.5f;

    public LevelData levelData;
    [Range(0, 3)]
    public float audioOffset = 1f;

    [Header("Button With Sound")]
    public GameObject[] prefabs;
    
    [Header("SFX")]
    public AudioSource audioClick;
    public AudioSource audioCorrect;
    public AudioSource audioFalse;
    public AudioSource audioStar;
    public AudioSource audioThings;
    public AudioSource audioLose;

    [Header("BGM")]
    [Range(2, 5)]
    public float decreaseAudioBGM = 2;
    public AudioSource audioBgm;
    public AudioClip[] bgmClip;
    
    private int indexBGM = -1;

    [HideInInspector] public float defaultVolumeBGM;

    private void Awake()
    {
        System.Random rand = new System.Random();

        float volume = SaveManager.LoadSoundData();
        int toggle = SaveManager.LoadEnableSoundData();
        
        bool toggleSound;

        if (toggle == 1) toggleSound = true;
        else toggleSound = false;

        SetVolumeSound(volume, toggleSound);
        indexBGM = rand.Next(0, bgmClip.Length);
        InitButtonSound();
    }

    private void InitButtonSound()
    {
        if (prefabs.Length < 1) return;

        foreach (GameObject prefab in prefabs)
        {
            int count = prefab.transform.childCount;

            if(count == 0)
            {
                prefab.gameObject.GetComponent<Button>().onClick.AddListener(() => {
                    PlayClickedAudio();
                });
                continue;
            }

            List<Button> buttons = new List<Button>();

            for (int i = 0; i < count; i++)
            {
                GameObject tmp = prefab.transform.GetChild(i).gameObject;

                if (tmp.CompareTag("ButtonClick"))
                {
                    buttons.Add(tmp.GetComponent<Button>());
                }
            }

            foreach (Button button in buttons)
            {
                button.onClick.AddListener(() =>
                {
                    PlayClickedAudio();
                });
            }

            buttons.Clear();
        }
    }

    public void PlayClickedAudio()
    {
        audioClick.Play();
    }

    public void PlayAudioThing(int index)
    {
        if (levelData == null) return;
        if (levelData.audioThingClip.Length < 1) return;

        audioThings.clip = levelData.audioThingClip[index];
        audioThings.Play();
    }

    public void PlayAudioCorrect()
    {
        audioCorrect.Play();
    }

    public void PlayAudioFalse()
    {
        audioFalse.Play();
    }

    public void PlayAudioStar()
    {
        audioStar.Play();
    }

    public bool IsAudioCorrectPlaying()
    {
        return audioCorrect.isPlaying;
    }

    public bool IsAudioThingsPlaying()
    {
        return audioThings.isPlaying;
    }

    public void PlayBGM()
    {
        if (audioBgm == null) return;

        if (!audioBgm.isPlaying)
        {
            indexBGM++;
            if (indexBGM >= bgmClip.Length) indexBGM = 0;
            audioBgm.clip = bgmClip[indexBGM];
            audioBgm.Play();
        }
    }

    public void SetVolumeBGM(float volume)
    {
        if(audioBgm != null)
            audioBgm.volume = volume;
    }
    
    public void PauseBGM()
    {
        Destroy(audioBgm);
    }

    public void PlayAudioLose()
    {
        audioLose.Play();
    }

    public float AudioLength(int index)
    {
        float length;

        length = audioClick.clip.length + audioCorrect.clip.length;

        return length;
    }

    public void SetVolumeSound(float volume, bool enableSound)
    {
        float previousVolume = 0;

        if (!enableSound)
        {
            previousVolume = volume;
            volume = 0;
        }

        audioClick.volume = volume;
        audioCorrect.volume = volume;
        audioStar.volume = volume;

        //audioThings.volume = volume;
        audioFalse.volume = volume;
        audioLose.volume = volume;
        audioBgm.volume = volume / 2;
        defaultVolumeBGM = volume / 2;

        if(!enableSound) SaveManager.SaveSoundData(previousVolume, enableSound);
        else SaveManager.SaveSoundData(volume, enableSound);
    }

    private void LateUpdate()
    {
        if (indexBGM == -1) return;
        PlayBGM();
    }
}
