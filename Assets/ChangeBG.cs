﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeBG : MonoBehaviour
{   
    public float timeChange;
    public Sprite[] backgroundImage;
    public Animator transitionAnimator;

    public Image image;
    private int index = 0;
    private float timer = 0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Timer();
    }

    void Timer()
    {
        timer += Time.deltaTime;

        if (timer >= timeChange)
        {   
            timer = 0f;
            ChangebackgroundImage();
        }
    }

    void ChangebackgroundImage()
    {
        image.sprite = backgroundImage[index];
        index++;

        if (index >= backgroundImage.Length)
        {
            index = 0;
        }
    }
}
