﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameplayUI : MonoBehaviour
{
    public float panelDuration = .5f;
    public float starsDuration = .5f;
    public AudioManager audioManager;

    [Header("Game UI")]
    public TextMeshProUGUI timerText;
    public TextMeshProUGUI findItemText;
    public Image itemImage;
    public TextMeshProUGUI itemName;
    public GameObject correctPanel;
    public GameObject falsePanel;
    public GameObject winPanel;
    public GameObject losePanel;

    [Header("Stars UI")]
    public Image[] starsImage;
    public Sprite[] starsLight;

    public void UpdateTimerUI(float timeGame)
    {
        timerText.text = Mathf.RoundToInt(timeGame).ToString();
    }


    public IEnumerator ShowCorrectPanel(Sprite itemImage, string itemName, float duration)
    {
        this.itemImage.sprite = itemImage;
        this.itemName.text = itemName;
        correctPanel.SetActive(true);
        yield return new WaitForSeconds(duration);
        correctPanel.SetActive(false);
    }

    public IEnumerator ShowFalsePanel()
    {
        falsePanel.SetActive(true);
        yield return new WaitForSeconds(panelDuration);
        falsePanel.SetActive(false);
    }

    public void SetFindItemTextFooter(string itemName)
    {
        findItemText.text = itemName;
    }

    public IEnumerator ShowWinPanel(int rating)
    {
        yield return new WaitForSeconds(2f);
        yield return new WaitForSeconds(panelDuration);
        winPanel.SetActive(true);

        for(int i = 0; i < rating; i++)
        {
            yield return new WaitForSeconds(starsDuration);
            starsImage[i].sprite = starsLight[i];
            audioManager.PlayAudioStar();
        }
    }

    public void ShowLosePanel()
    {
        losePanel.SetActive(true);
    }
}
